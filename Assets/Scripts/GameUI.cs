using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Unavia.SimpleClocks
{
    public class GameUI : MonoBehaviour
    {
        #region Properties
        private Button QuitButton;
        #endregion


        #region Unity Methods
        void Start()
        {

        }

        void OnEnable()
        {
            VisualElement uiRoot = GetComponent<UIDocument>().rootVisualElement;

            QuitButton = uiRoot.Q<Button>("QuitButton");
            QuitButton.RegisterCallback<ClickEvent>(ev => Quit());
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Close the app
        /// </summary>
        private void Quit()
        {
            Application.Quit();

            #if UNITY_EDITOR
            EditorApplication.ExitPlaymode();
            #endif
        }
        #endregion
    }
}

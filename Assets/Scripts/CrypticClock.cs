﻿using System;
using UnityEngine;

namespace Unavia.SimpleClocks
{
    public enum ClockMode
    {
        /// <summary>
        /// Input time
        /// </summary>
        CUSTOM,
        /// <summary>
        /// System time
        /// </summary>
        SYSTEM,
        /// <summary>
        /// Fixed time
        /// </summary>
        FIXED
    }


    public class CrypticClock : MonoBehaviour
    {
        #region Fields
        public ClockMode Mode = ClockMode.SYSTEM;
        public bool Paused = false;

        [Header("Indicators")]
        [SerializeField]
        private GameObject center = default;
        [SerializeField]
        private GameObject hoursInnerParent = default;
        [SerializeField]
        private GameObject hoursOuterParent = default;
        [SerializeField]
        private GameObject minutesInnerParent = default;
        [SerializeField]
        private GameObject minutesOuterParent = default;
        [SerializeField]
        private GameObject secondsParent = default;
        #endregion

        // NOTE: Updates per second should remain low (performance)
        private int updatesPerSecond = 4;
        // Allow using/tracking a custom/scaled time
        private DateTime pseudoTime;
        private float pseudoTimeScale = 1f;
        private float timeSinceLastUpdate = 0f;


        #region Unity Methods
        void Start()
        {
            pseudoTime = DateTime.Now;
        }

        void Update()
        {
            if (Paused) return;

            // Fixed clocks should never update internally
            if (Mode == ClockMode.FIXED) return;

            timeSinceLastUpdate += Time.deltaTime;
            // Only update the clock a set amount of times per second
            if (timeSinceLastUpdate <= 1f / updatesPerSecond) return;

            // System clocks should update according to system time (and updates per second)
            if (Mode == ClockMode.SYSTEM)
            {
                UpdateClock(DateTime.Now);
            }
            else if (Mode == ClockMode.CUSTOM)
            {
                pseudoTime = pseudoTime.AddSeconds(pseudoTimeScale * timeSinceLastUpdate);
                UpdateClock(pseudoTime);
            }

            timeSinceLastUpdate = 0f;
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Pause the clock
        /// </summary>
        public void Pause()
        {
            if (Paused) return;

            Paused = true;
        }

        /// <summary>
        /// Resume the clock
        /// </summary>
        public void Resume()
        {
            if (!Paused) return;

            Paused = false;
        }

        /// <summary>
        /// Resume the clock with a custom time
        /// </summary>
        /// <param name="time">Custom time</param>
        /// <remarks>Only affects custom clocks</remarks>
        public void Resume(DateTime time, float timeScale = 1f)
        {
            if (!Paused) return;

            // Clamp custom time scale (up to 1 hr per second)
            timeScale = Mathf.Clamp(timeScale, 0.1f, 3600f);

            pseudoTime = time;
            pseudoTimeScale = timeScale;

            Resume();
        }

        /// <summary>
        /// Change the clock mode
        /// </summary>
        /// <param name="mode">Clock mode</param>
        /// <param name="updateTime">Whether pseudo time should be updated</param>
        public void ChangeMode(ClockMode mode, bool updateTime = true)
        {
            ChangeMode(mode, updateTime ? DateTime.Now : pseudoTime);
        }

        /// <summary>
        /// Change the clock mode
        /// </summary>
        /// <param name="mode">Clock mode</param>
        /// <param name="time">Clock time</param>
        /// <remarks>Custom time is not used for system mode</remarks>
        public void ChangeMode(ClockMode mode, DateTime time)
        {
            Mode = mode;
            pseudoTime = time;
        }

        private void UpdateClock(DateTime time)
        {
            // Center indicates whether seconds are even (unlit) or odd (lit)
            bool areSecondsEven = time.Second % 2 == 0;
            center.SetActive(!areSecondsEven);

            // Seconds are indicated by the outer ring and center
            int secondsIndex = time.Second / 2;
            for (int i = 0; i < secondsParent.transform.childCount; i++)
            {
                secondsParent.transform.GetChild(i).gameObject.SetActive(i < secondsIndex);
            }

            // Minutes are indicated by the next two rings
            int minutesOutterIndex = time.Minute % 5;
            int minutesInnerIndex = time.Minute / 5;
            for (int i = 0; i < minutesOuterParent.transform.childCount; i++)
            {
                minutesOuterParent.transform.GetChild(i).gameObject.SetActive(i < minutesOutterIndex);
            }
            for (int i = 0; i < minutesInnerParent.transform.childCount; i++)
            {
                minutesInnerParent.transform.GetChild(i).gameObject.SetActive(i < minutesInnerIndex);
            }

            // Hours are indicated by the inner two rings
            int hoursOuterIndex = time.Hour % 5;
            int hoursInnerIndex = time.Hour / 5;
            for (int i = 0; i < hoursOuterParent.transform.childCount; i++)
            {
                hoursOuterParent.transform.GetChild(i).gameObject.SetActive(i < hoursOuterIndex);
            }
            for (int i = 0; i < hoursInnerParent.transform.childCount; i++)
            {
                hoursInnerParent.transform.GetChild(i).gameObject.SetActive(i < hoursInnerIndex);
            }
        }
        #endregion
    }
}
